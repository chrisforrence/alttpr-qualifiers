<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $races = [
            'curious-ganonstower-8179',
            'dynamic-swampofevil-5146',
            'lawful-swamp-0475',
            'graceful-compass-3740',
            'odd-icerod-7849',
            'secret-mothlua-1629',
            'adequate-hypecave-2677',
            'powerful-flippers-9661',
        ];
        foreach ($races as $race) {
            Artisan::call('process:results', ['slug' => $race]);
        }
    }
}
