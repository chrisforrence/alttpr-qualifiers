<?php

namespace App\Http\Controllers;

use App\Models\Entrant;
use Illuminate\Http\Request;

class EntrantController extends Controller
{
    public function show(Entrant $entrant)
    {
        return response()->view('entrants.show', ['entrant' => $entrant]);
    }
}
