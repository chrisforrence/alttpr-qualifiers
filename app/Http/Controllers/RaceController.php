<?php

namespace App\Http\Controllers;

use App\Models\Race;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;

class RaceController extends Controller
{
    public function index(): Response
    {
        $races = Cache::remember('races', 86400, function () {
            return Race::orderBy('started_at')->get();
        });
        return response()->view('races.index', ['races' => $races]);
    }

    public function show(Race $race): Response
    {
        $race->load('results');
        return response()->view('races.show', ['race' => $race]);
    }
}
