<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'estimate' => [
        'rank' => 'Showing estimate, actual rank would be :current_rank',
        'score' => 'Showing estimate, actual score would be :current_score',
    ]

];
