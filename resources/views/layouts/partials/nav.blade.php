@php
$nav = [
    'results' => ['label' => '2022 Standings', 'matches' => ['/', 'entrants*']],
    'races.index' => ['label' => 'Races', 'matches' => ['races*']],
];
@endphp
<nav class="bg-gray-800">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="border-b border-gray-700">
            <div class="flex items-center justify-between h-16 px-4 sm:px-0">
                <div class="flex items-center">
                    <div class="flex-shrink-0">
                        <a href="/">
                            <svg class="h-8 w-8 text-white fill-current" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="M256 59.72L142.687 256h226.625L256 59.72zM369.313 256L256 452.28h226.625L369.312 256zM256 452.28L142.687 256 29.376 452.28H256z"/></svg>
                        </a>
                    </div>
                    <div class="hidden md:block">
                        <div class="ml-10 flex items-baseline space-x-4">
                            @foreach($nav as $route => $entry)
                                <a href="{{ route($route) }}" class="
                                    {{ request()->is(...$entry['matches']) ? 'bg-gray-900 text-white' : 'text-gray-300 hover:bg-gray-700 hover:text-white' }}
                                    px-3
                                    py-2
                                    rounded-md
                                    text-sm
                                    font-medium"
                                   aria-current="page">
                                    {{ $entry['label'] }}
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="hidden md:block">
                    <div class="ml-4 flex items-center md:ml-6">
                        <a href="https://discord.gg/9vBRAMA28j" class="bg-gray-900 text-white px-3 py-2 rounded-md text-sm font-medium">Join Discord</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Mobile menu, show/hide based on menu state. -->
    <div class="border-b border-gray-700 md:hidden" id="mobile-menu">
        <div class="px-2 py-3 space-y-1 sm:px-3">
            @foreach($nav as $route => $entry)
                <a href="{{ route($route) }}" class="
                                    {{ request()->is(...$entry['matches']) ? 'bg-gray-900 text-white' : 'text-gray-300 hover:bg-gray-700 hover:text-white' }}
                    px-3
                    py-2
                    rounded-md
                    text-base
                    font-medium
                    block"
                   aria-current="page">
                    {{ $entry['label'] }}
                </a>
            @endforeach
                <a href="https://discord.gg/9vBRAMA28j" class="text-gray-300 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-medium">Join Discord!</a>
        </div>
    </div>
</nav>
