<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('page_title', config('app.name'))</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">

    <!-- Scripts -->
    <script src="{{ mix('/js/app.js') }}"></script>
</head>
<body class="antialiased">
<div class="min-h-screen bg-gray-200">
    <!-- This example requires Tailwind CSS v2.0+ -->
    <div>
        <div class="bg-gray-800 pb-32">
            @include('layouts.partials.nav')
            <header class="py-10">
                <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                    <h1 class="text-3xl font-bold text-white">
                        @yield('title')
                    </h1>
                </div>
            </header>
        </div>

        <main class="-mt-32">
            <div class="max-w-7xl mx-auto pb-12 px-4 sm:px-6 lg:px-8">
                <!-- Replace with your content -->
                    @yield('contents')
                <!-- /End replace -->
            </div>
        </main>
    </div>

    <footer class="bg-white">
        <div class="max-w-7xl mx-auto py-12 px-4 sm:px-6 md:flex md:items-center md:justify-between lg:px-8">
            <div class="mt-8 md:mt-0 md:order-1">
                <p class="text-center text-base text-gray-400">
                    Site by <a href="https://twitch.tv/sorcererfinch" target="_blank">SorcererFinch</a>. Data sourced from <a href="https://racetime.gg/alttpr">racetime.gg</a>, results are not official, and this site is not endorsed in any way by the folks who run the ALttPR races.
                </p>
            </div>
        </div>
    </footer>
    @yield('scripts')

    @if(config('services.fathom'))
    <!-- Fathom - beautiful, simple website analytics -->
    <script src="https://cdn.usefathom.com/script.js" data-site="{{ config('services.fathom') }}" defer></script>
    <!-- / Fathom -->
    @endif
</div>
</body>
</html>
