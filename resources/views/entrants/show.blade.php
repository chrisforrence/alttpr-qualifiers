@extends('layouts.app')

@section('page_title', $entrant->name . ' | ' . config('app.name'))

@section('nav')
<!-- This example requires Tailwind CSS v2.0+ -->
<nav class="flex" aria-label="Breadcrumb">
    <ol class="flex items-center space-x-4">
        <li>
            <div>
                <a href="{{ route('results.estimated') }}" class="text-black hover:text-gray-800">
                    <!-- Heroicon name: solid/home -->
                    <svg class="flex-shrink-0 h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                        <path d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z" />
                    </svg>
                    <span class="sr-only">Home</span>
                </a>
            </div>
        </li>

        <li>
            <div class="flex items-center">
                <!-- Heroicon name: solid/chevron-right -->
                <svg class="flex-shrink-0 h-5 w-5 text-black" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                </svg>
                <a href="{{ route('entrants.show', [$entrant]) }}" class="ml-4 text-sm font-medium text-black hover:text-gray-800">{{ $entrant->name }}</a>
            </div>
        </li>
    </ol>
</nav>
@endsection

@section('contents')
    <dl class="mt-5 grid grid-cols-1 gap-5 sm:grid-cols-3 pb-8">

        <div class="px-4 py-5 bg-white shadow rounded-lg overflow-hidden sm:p-6">
            <dt class="text-sm font-medium text-gray-500 truncate">
                {{ $entrant->use_estimate ? 'Estimated' : '' }} Rank
            </dt>
            <dd class="mt-1 text-3xl font-semibold text-gray-900">
                @if($entrant->use_estimate)
                    <span title="{{ __('alttpr.estimate.rank', ['current_rank' => $entrant->current_rank]) }}">
                        {{ round($entrant->rank, 2) }} **
                    </span>
                @else
                    {{ $entrant->rank }}
                @endif
                @if($entrant->rank <= config('app.qualifying_racer_count'))
                    <span class="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-blue-100 text-blue-800">
                        @if($entrant->use_estimate) Conditionally @endif Qualifies
                    </span>
                @else
                    <span class="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-red-100 text-red-800">Not Qualified</span>
                @endif
            </dd>
        </div>
        <div class="px-4 py-5 bg-white shadow rounded-lg overflow-hidden sm:p-6">
            <dt class="text-sm font-medium text-gray-500 truncate">
                {{ $entrant->use_estimate ? 'Estimated' : '' }} Score
            </dt>
            <dd class="mt-1 text-3xl font-semibold text-gray-900">
                @if($entrant->use_estimate)
                    <span title="{{ __('alttpr.estimate.score', ['current_score' => round($entrant->current_score, 2)]) }}">
                        {{ round($entrant->score, 2) }} **
                    </span>
                @else
                    {{ round($entrant->score, 2) }}
                @endif
            </dd>
        </div>

        <div class="px-4 py-5 bg-white shadow rounded-lg overflow-hidden sm:p-6">
            <dt class="text-sm font-medium text-gray-500 truncate">
                Races Run
            </dt>
            <dd class="mt-1 text-3xl font-semibold text-gray-900">
                {{ count($entrant->results) }}
                @if(count($entrant->results) < config('app.minimum_races'))
                    <span class="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-red-100 text-red-800">
                        Does not yet meet minimum race threshold
                    </span>
                @endif
            </dd>
        </div>

    </dl>
    <div class="flex flex-col">
        <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                    <table class="min-w-full divide-y divide-gray-200">
                        <thead class="bg-green-900 text-green-50">
                        <tr>
                            <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-green-50 uppercase tracking-wider">
                                Race
                            </th>
                            <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-green-50 uppercase tracking-wider">
                                Finish Time
                            </th>
                            <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-green-50 uppercase tracking-wider">
                                Score
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($entrant->results()->with('race')->get()->sortBy('race.started_at') as $result)
                            <tr class="{{ $loop->even ? 'bg-gray-50' : 'bg-white' }}">
                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                    <p><a class="font-medium" href="{{ route('races.show', [$result->race]) }}">{{ $result->race->slug }}</a></p>
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                    {{ $result->is_finished ? $result->time : 'DNF' }}
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                    {{ $result->score }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
