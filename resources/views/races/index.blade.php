@extends('layouts.app')

@section('page_title', 'Races | ' . config('app.name'))

@section('nav')
    <!-- This example requires Tailwind CSS v2.0+ -->
    <nav class="flex" aria-label="Breadcrumb">
        <ol class="flex items-center space-x-4">
            <li>
                <div>
                    <a href="{{ route('results.estimated') }}" class="text-black hover:text-gray-800">
                        <!-- Heroicon name: solid/home -->
                        <svg class="flex-shrink-0 h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                            <path d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z" />
                        </svg>
                        <span class="sr-only">Home</span>
                    </a>
                </div>
            </li>

            <li>
                <div class="flex items-center">
                    <!-- Heroicon name: solid/chevron-right -->
                    <svg class="flex-shrink-0 h-5 w-5 text-black" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                        <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                    </svg>
                    <a href="{{ route('races.index') }}" class="ml-4 text-sm font-medium text-black hover:text-gray-800">Races</a>
                </div>
            </li>
        </ol>
    </nav>
@endsection

@section('contents')
    <div class="flex flex-col">
        <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                    <table class="min-w-full divide-y divide-gray-200">
                        <thead class="bg-green-900 text-green-50">
                        <tr>
                            <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-green-50 uppercase tracking-wider">
                                Qualifier #
                            </th>
                            <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-green-50 uppercase tracking-wider">
                                Slug
                            </th>
                            <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-green-50 uppercase tracking-wider">
                                Race Date
                            </th>
                            <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-green-50 uppercase tracking-wider">
                                Seed
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($races as $race)
                            <tr class="{{ $loop->even ? 'bg-gray-50' : 'bg-white' }}">
                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                    {{ $loop->iteration }}
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                    <p><a class="font-medium" href="{{ route('races.show', [$race]) }}">{{ $race->slug }}</a></p>
                                </td>
                                <td id="js-race-started-at-{{ $race->slug }}" class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                    {{ $race->started_at }}
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                    <a href="{{ $race->seed }}" target="_blank">{{ $race->seed }}</a>
                                </td>
                                <script>
                                    document.getElementById('js-race-started-at-{{ $race->slug }}').innerText
                                        = moment.tz('{{ $race->started_at }}', 'UTC').tz(moment.tz.guess()).format('MMMM Do, YYYY, h:mma z');
                                </script>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
