<?php

use App\Http\Controllers\EntrantController;
use App\Http\Controllers\OauthController;
use App\Http\Controllers\QualifierController;
use App\Http\Controllers\RaceController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 |--------------------------------------------------------------------------
 | Public Routes
 |--------------------------------------------------------------------------
 */
Route::get('/', [QualifierController::class, config('app.use_estimates') ? 'estimated' : 'actual'])->name('results');
Route::get('/estimate', [QualifierController::class, 'estimated'])->name('results.estimated');
Route::get('/actual', [QualifierController::class, 'actual'])->name('results.actual');
Route::get('/entrants/{entrant:slug}', [EntrantController::class, 'show'])->name('entrants.show');
Route::get('/races', [RaceController::class, 'index'])->name('races.index');
Route::get('/races/{race:slug}', [RaceController::class, 'show'])->name('races.show');

Route::get('/login/with/{provider}', [OauthController::class, 'redirectToProvider'])->name('oauth.redirect');
Route::get('/login/with/{provider}/callback', [OauthController::class, 'handleCallback'])->name('oauth.callback');

