# ALTTPR Qualifiers Code

This application handles processing, storing, and displaying race results from racetime.gg for the Legend of Zelda: A Link To The Past Randomizer Tournament Qualifiers!

## Requirements

The following is required to be installed on the server that this application will be running:

- PHP 8.1 or greater
- [Composer](https://getcomposer.org) v2
- nginx or Apache HTTP Server

In addition, the server also needs to have access to:

- a relational database (e.g. PostgreSQL, MariaDB, MySQL),
- a cache (e.g. redis, memcache), and
- a queue service (e.g. redis)

## Installation

```
git clone https://gitlab.com/chrisforrence/alttpr-qualifiers.git /path/to/installation
cd /path/to/installation
cp .env.example .env
```

From here, edit the .env file to fill in the database credentials and connection information, as well as the queue and cache information.

```
php artisan key:generate
php artisan migrate
```

If you would like to reset the database in its entirety, you can run the `migrate:fresh` command instead:

```
php artisan migrate:fresh
```

## Usage

To process a race either already completed or in progress:

```
php artisan process:results <race-slug-4439>
```

This will calculate a par time given the top five finishers, apply scores to all of the finishers in that race (including DNF/DQ), and re-cache the information. If the race has not yet finished, the command will be automatically re-run every ten minutes until the race concludes.

To adjust an entrant's time:

```
php artisan adjust:results {race} {racer} {time}
# Example: php artisan adjust:results swag-armosknight-5959 SorcererFinch#7887 2:01:58
```

To disqualify an entrant from a race:

```
php artisan adjust:results {race} {racer} DQ
```
